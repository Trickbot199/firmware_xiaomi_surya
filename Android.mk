LOCAL_PATH:= $(call my-dir)

ifneq ($(filter surya,$(TARGET_DEVICE)),)

$(info "Including Blobs from V12.5.7.0.RJGMIXM")

# firmware
$(call add-radio-file,abl.elf)
$(call add-radio-file,aop.mbn)
$(call add-radio-file,BTFM.bin)
$(call add-radio-file,cmnlib.mbn)
$(call add-radio-file,cmnlib64.mbn)
$(call add-radio-file,devcfg.mbn)
$(call add-radio-file,dspso.bin)
$(call add-radio-file,ffu.img)
$(call add-radio-file,hyp.mbn)
$(call add-radio-file,imagefv.elf)
$(call add-radio-file,km4.mbn)
$(call add-radio-file,NON-HLOS.bin)
$(call add-radio-file,qupv3fw.elf)
$(call add-radio-file,storsec.mbn)
$(call add-radio-file,tz.mbn)
$(call add-radio-file,uefi_sec.mbn)
$(call add-radio-file,xbl.elf)
$(call add-radio-file,xbl_config.elf)

endif
